#include "common.h"

void main(){

	init_hardware();
	init_systick();
	init_rgb();

	while(1);
}

// Aktiviere PORTJ-Buttons mit Interrupt-Handler
void init_hardware(){
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOJ);

    ROM_GPIOPinTypeGPIOInput(GPIO_PORTJ_BASE, GPIO_PIN_0);
    ROM_GPIOPinTypeGPIOInput(GPIO_PORTJ_BASE, GPIO_PIN_1);

    ROM_GPIOIntEnable(GPIO_PORTJ_BASE, GPIO_PIN_0);
    ROM_GPIOIntEnable(GPIO_PORTJ_BASE, GPIO_PIN_1);

    ROM_GPIOIntTypeSet(GPIO_PORTJ_BASE, GPIO_PIN_0, GPIO_FALLING_EDGE);
    ROM_GPIOIntTypeSet(GPIO_PORTJ_BASE, GPIO_PIN_1, GPIO_FALLING_EDGE);

    GPIOIntRegister(GPIO_PORTJ_BASE, add_duty);

    GPIOPadConfigSet(GPIO_PORTJ_BASE, GPIO_PIN_0, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);
    GPIOPadConfigSet(GPIO_PORTJ_BASE, GPIO_PIN_1, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);
}


// Aktiviere SysTick für Software-PWM
void init_systick(){

	SysCtlClockFreqSet(SYSCTL_XTAL_25MHZ | SYSCTL_USE_PLL | SYSCTL_CFG_VCO_480, TAKTFREQUENZ);
	ROM_SysTickEnable();
	SysTickIntRegister(next_cycle);
	ROM_SysTickPeriodSet(1000);
	ROM_SysTickIntEnable();
}

// Aktiviere RGB-LEDs auf dem Color Click
void init_rgb(){

	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);			//Red LED Port
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOP);			//Green LED Port
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOM);			//Blue LED Port

	ROM_GPIOPinTypeGPIOOutput(GPIO_PORTD_BASE, GPIO_PIN_2);		//Red LED Pin
	ROM_GPIOPinTypeGPIOOutput(GPIO_PORTP_BASE, GPIO_PIN_5);		//Green LED Pin
	ROM_GPIOPinTypeGPIOOutput(GPIO_PORTM_BASE, GPIO_PIN_7);		//Blue LED Pin
}

//SysTick Interrupt-Handler
void next_cycle(void) {
	GPIOPinWrite(red_led_port, red_led_pin, 0);
	GPIOPinWrite(green_led_port, green_led_pin, 0);
	GPIOPinWrite(blue_led_port, blue_led_pin, 0);

	if(loopCount < STEPS){
		loopCount++;
	}else{
		loopCount = 0;
	}

	if(loopCount < duty_red){
		GPIOPinWrite(red_led_port, red_led_pin, red_led_pin);
	}

	if(loopCount < duty_green){
		GPIOPinWrite(green_led_port, green_led_pin, green_led_pin);
	}

	if(loopCount < duty_blue){
		GPIOPinWrite(blue_led_port, blue_led_pin, blue_led_pin);
	}

}

//PORTJ_Button Interrupt-Handler
void add_duty(void) {

	if (ROM_GPIOIntStatus(button_port, false) & button_pin_2){
		if(currentLED < 3){
			currentLED++;
		}else{
			currentLED = 0;
		}
	}else if(ROM_GPIOIntStatus(button_port, false) & button_pin){
		switch(currentLED){
			case 0:
				if(duty_red < STEPS){
					duty_red += STEP;
				}
				else{
					duty_red = 0;
				}
				break;
			case 1:
				if(duty_green < STEPS){
					duty_green += STEP;
				}
				else{
					duty_green = 0;
				}
				break;
			case 2:
				if(duty_blue < STEPS){
					duty_blue += STEP;
				}
				else{
					duty_blue = 0;
				}
				break;
			default:
				currentLED = 0;
				break;
		}
	}

	GPIOIntClear(button_port_2, button_pin_2);
    GPIOIntClear(button_port, button_pin);
}

