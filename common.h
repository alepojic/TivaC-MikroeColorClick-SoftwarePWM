
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <inc/hw_memmap.h>
#include <inc/hw_types.h>
#include <driverlib/gpio.h>
#include <driverlib/pin_map.h>
#include <driverlib/interrupt.h>
#include <driverlib/systick.h>
#include <driverlib/sysctl.h>
#include <driverlib/adc.h>
#include <driverlib/uart.h>
#include <driverlib/i2c.h>

#define TARGET_IS_TM4C129_RA0
#include <driverlib/rom.h>

#define STEPS 100
#define STEP 10

#define SLAVE_ADDR 0x39
#define delay_us(x) SysCtlDelay(TAKTFREQUENZ/1000000/3*x)
#define TAKTFREQUENZ 120000000

#define button_port GPIO_PORTJ_BASE         // USR_SW1 Port
#define button_pin GPIO_PIN_0               // USR_SW1 Pin
#define button_port_2 GPIO_PORTJ_BASE		// USR_SW2 Port
#define button_pin_2 GPIO_PIN_1				// USR_SW2 Pin

#define red_led_port GPIO_PORTD_BASE
#define red_led_pin GPIO_PIN_2
#define blue_led_port GPIO_PORTM_BASE
#define blue_led_pin GPIO_PIN_7
#define green_led_port GPIO_PORTP_BASE
#define green_led_pin GPIO_PIN_5

volatile int currentLED = 0;
volatile int loopCount = 0;
volatile int duty_red = 0;
volatile int duty_green = 0;
volatile int duty_blue = 0;

void init_hardware();
void init_systick();
void init_i2c();
void init_rgb();
void add_duty();
void next_cycle();
